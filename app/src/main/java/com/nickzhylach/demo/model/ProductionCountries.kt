package com.nickzhylach.demo.model

data class ProductionCountries(
    val iso_3166_1 : String,
    val name : String
)
