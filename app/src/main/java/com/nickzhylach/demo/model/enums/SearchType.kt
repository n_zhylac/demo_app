package com.nickzhylach.demo.model.enums

enum class SearchType {
    SEARCH, LAST
}