package com.nickzhylach.demo.api

import com.nickzhylach.demo.common.API_KEY
import com.nickzhylach.demo.model.Movie
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Query

interface Api {
    @GET("/3/movie/latest")
    fun getLatestMovieAsync(@Query("api_key") apiKey: String = API_KEY): Deferred<Movie>
}