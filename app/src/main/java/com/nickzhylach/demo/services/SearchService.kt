package com.nickzhylach.demo.services

import com.nickzhylach.demo.api.Api
import com.nickzhylach.demo.common.API_KEY
import com.nickzhylach.demo.model.Movie
import retrofit2.Retrofit
import javax.inject.Inject

class SearchService @Inject constructor(private val retrofit: Retrofit): ISearchService {
    private val api = retrofit.create(Api::class.java)

    override suspend fun searchByActor(actor: String): List<Movie> {
        TODO("Not yet implemented")
    }

    override suspend fun getLastAsync(): Movie? {
        return try {
            api.getLatestMovieAsync(API_KEY).await().apply {
                this.adult
            }
        } catch (e: Exception) {
            null
        }
    }
}