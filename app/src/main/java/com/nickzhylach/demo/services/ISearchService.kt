package com.nickzhylach.demo.services

import com.nickzhylach.demo.model.Movie

interface ISearchService {
    suspend fun searchByActor(actor: String): List<Movie>
    suspend fun getLastAsync(): Movie?
}