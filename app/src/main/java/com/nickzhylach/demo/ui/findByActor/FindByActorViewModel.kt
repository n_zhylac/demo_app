package com.nickzhylach.demo.ui.findByActor

import androidx.databinding.ObservableField
import com.nickzhylach.demo.ui.common.BaseViewModel
import javax.inject.Inject

class FindByActorViewModel @Inject constructor(): BaseViewModel() {
    val actorName = ObservableField<String>()

    fun searchByName() {}
}