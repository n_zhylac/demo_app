package com.nickzhylach.demo.ui.findByActor

import com.nickzhylach.demo.ui.common.BaseFragment
import com.nickzhylach.demo.R
import com.nickzhylach.demo.databinding.FFindByBinding

class FindByFragment: BaseFragment<FFindByBinding>() {
    override val layout: Int = R.layout.f_find_by
}