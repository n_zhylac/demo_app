package com.nickzhylach.demo.ui.common

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.nickzhylach.demo.common.ToastManager
import dagger.android.support.DaggerFragment

@Suppress("UNCHECKED_CAST")
abstract class BaseFragment<Binding: ViewDataBinding>: DaggerFragment() {
    abstract val layout: Int

    private lateinit var binding: ViewDataBinding

    protected fun getBinding() = binding as Binding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        this.binding = DataBindingUtil.inflate<Binding>(inflater, layout, container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        return this.binding.root
    }

    protected fun <Y> LiveData<CallbackEvent<Y>>.subscribe(action: (value: Y) -> Unit) = this.observe(viewLifecycleOwner, Observer { event ->
        event?.getContentIfNotHandledOrReturnNull()?.let { action(it) }
    })

    fun showMessage(message: String) = ToastManager.showToast(requireContext(), message)

    fun showMessage(message: Int) = ToastManager.showToast(requireContext(), message)
}