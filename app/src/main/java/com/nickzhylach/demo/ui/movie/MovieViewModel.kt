package com.nickzhylach.demo.ui.movie

import androidx.databinding.ObservableField
import androidx.lifecycle.viewModelScope
import com.nickzhylach.demo.model.Movie
import com.nickzhylach.demo.services.ISearchService
import com.nickzhylach.demo.ui.common.BaseViewModel
import com.nickzhylach.demo.ui.common.CallbackEvent
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

class MovieViewModel @Inject constructor(private val searchService: ISearchService): BaseViewModel() {
    val movieField = ObservableField<Movie?>()

    fun setMovie(movie: Movie) {
        movieField.set(movie)
    }

    fun showMovie(id: Int) {
    }

    fun showLastMovie() {
        viewModelScope.launch(Dispatchers.IO) {
            searchService.getLastAsync()?.let {
                setMovie(it)
            }?: kotlin.run {
                _messageLiveData.postValue(CallbackEvent("Something Went Wrong!"))
            }
        }
    }
}