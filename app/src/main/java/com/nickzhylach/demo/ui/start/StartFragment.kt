package com.nickzhylach.demo.ui.start

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.nickzhylach.demo.ui.common.BaseFragment
import com.nickzhylach.demo.R
import com.nickzhylach.demo.dagger.ViewModelFactory
import com.nickzhylach.demo.databinding.FStartBinding
import com.nickzhylach.demo.model.enums.SearchType
import javax.inject.Inject

class StartFragment: BaseFragment<FStartBinding>() {
    override val layout: Int = R.layout.f_start

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private val viewModel: StartViewModel by viewModels { viewModelFactory }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.searchType.subscribe {
            when(it) {
                SearchType.SEARCH -> {
                    findNavController().navigate(StartFragmentDirections.actionStartFragmentToFindByActorFragment())
                }
                SearchType.LAST -> {
                    findNavController().navigate(StartFragmentDirections.actionStartFragmentToMovieFragment())
//                    showMessage("Something went wrong!")
                }
            }
        }

        getBinding().viewModel = viewModel
    }
}