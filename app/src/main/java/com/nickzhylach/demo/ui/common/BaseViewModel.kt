package com.nickzhylach.demo.ui.common

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

abstract class BaseViewModel: ViewModel() {
    protected val _messageLiveData = MutableLiveData<CallbackEvent<String>>()
    val messageLiveData = _messageLiveData as LiveData<CallbackEvent<String>>
    protected val _messageIdLiveData = MutableLiveData<CallbackEvent<Int>>()
    val messageIdLiveData = _messageIdLiveData as LiveData<CallbackEvent<Int>>
    protected val _backLiveData = MutableLiveData<CallbackEvent<Any>>()
    val backLiveData = _backLiveData as LiveData<CallbackEvent<Any>>
    fun back() {
        _backLiveData.postValue(CallbackEvent(1))
    }
}