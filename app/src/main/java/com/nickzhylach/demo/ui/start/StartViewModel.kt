package com.nickzhylach.demo.ui.start

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.nickzhylach.demo.model.enums.SearchType
import com.nickzhylach.demo.ui.common.BaseViewModel
import com.nickzhylach.demo.ui.common.CallbackEvent
import javax.inject.Inject

class StartViewModel @Inject constructor(): BaseViewModel() {
    private val _searchLiveData = MutableLiveData<CallbackEvent<SearchType>>()

    val searchType = _searchLiveData as LiveData<CallbackEvent<SearchType>>

    fun last() {
        _searchLiveData.postValue(CallbackEvent(SearchType.LAST))
    }

    fun search() {
        _searchLiveData.postValue(CallbackEvent(SearchType.SEARCH))
    }
}