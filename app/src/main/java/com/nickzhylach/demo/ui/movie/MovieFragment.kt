package com.nickzhylach.demo.ui.movie

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.nickzhylach.demo.databinding.FMovieBinding
import com.nickzhylach.demo.ui.common.BaseFragment
import com.nickzhylach.demo.R
import com.nickzhylach.demo.dagger.ViewModelFactory
import javax.inject.Inject

class MovieFragment: BaseFragment<FMovieBinding>() {
    override val layout: Int = R.layout.f_movie

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private val viewModel: MovieViewModel by viewModels { viewModelFactory }
//    private val args: MovieFragmentArgs by navArgs()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

//        args.movieId?.let {
//            viewModel.showMovie(it)
//        }?: kotlin.run {
            viewModel.showLastMovie()
//        }

        viewModel.messageLiveData.subscribe {
            showMessage(it)
        }

        getBinding().viewModel = viewModel
    }
}