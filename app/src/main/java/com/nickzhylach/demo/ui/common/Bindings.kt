package com.nickzhylach.demo.ui.common

import android.net.Uri
import android.text.Spannable
import android.text.SpannableString
import android.text.style.UnderlineSpan
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.core.text.HtmlCompat
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions

@BindingAdapter("circleImageRes")
fun setCircleImageRes(imageView: ImageView, imageResId: Int) {
    var requestOptions = RequestOptions()
    requestOptions = requestOptions.transform(CenterCrop(), RoundedCorners(imageView.rootView.width / 2))
    Glide.with(imageView.context)
        .load(imageResId)
        .apply(requestOptions)
        .into(imageView)
}

@BindingAdapter("textById")
fun setTextById(textView: TextView, resId: Int?) {
    resId?.let { textView.setText(it) }?: run { textView.text = "" }
}

@BindingAdapter("textByString")
fun setTextByString(textView: TextView, text: String?) {
    text?.let {
        if (textView.text.toString() != text)
        textView.text = it
    }?: run {
        textView.text = ""
    }
}

@BindingAdapter("spannedTextById")
fun setSpannedTextById(view: TextView, resId: Int?) {
    resId?.let {
        val string = view.context.getString(it)
        val ss = SpannableString(string)
        ss.setSpan(UnderlineSpan(), 0, string.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        view.text = ss
    }?: run { view.text = "" }
}

@BindingAdapter("imageUri")
fun loadImageByUri(iv: ImageView, uri: Uri?) {
    if (uri != null) {
        Glide.with(iv.context)
                .load(uri)
                .into(iv)
    } else {
        iv.setImageDrawable(null)
    }
}

@BindingAdapter("imageUrl")
fun loadImageByUrl(iv: ImageView, urlPart: String?) {
    var requestOptions = RequestOptions()
    val radius = iv.width / 10
    requestOptions = requestOptions.transform(CenterCrop(), RoundedCorners(if (radius > 0) radius else 10))
    if (urlPart != null) {
        Glide.with(iv.context)
            .load("https://image.tmdb.org/t/p/w500${urlPart}")
            .apply(requestOptions)
            .into(iv)
    } else {
        Glide.with(iv.context)
            .load("https://image.tmdb.org/t/p/w500/6vcDalR50RWa309vBH1NLmG2rjQ.jpg")
            .apply(requestOptions)
            .into(iv)
        iv.setImageDrawable(null)
    }
}

@BindingAdapter("htmlText")
fun setHtmlText(textView: TextView, text: Int?) {
    text?.let {
        textView.setText(
            HtmlCompat.fromHtml(
                textView.context.getString(it),
                HtmlCompat.FROM_HTML_MODE_LEGACY
            ),
            TextView.BufferType.SPANNABLE
        )
    }
}

@BindingAdapter("inputEnable")
fun setInputEnable(et: EditText, enable: Boolean) {
    with(et) {
        isClickable = enable
        isEnabled = enable
    }
}