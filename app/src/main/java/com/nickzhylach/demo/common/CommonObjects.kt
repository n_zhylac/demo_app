package com.nickzhylach.demo.common

import android.content.Context
import android.widget.Toast
import java.text.SimpleDateFormat
import java.util.*
import android.content.BroadcastReceiver
import android.content.Intent
import android.content.IntentFilter
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.LifecycleOwner
import androidx.localbroadcastmanager.content.LocalBroadcastManager

const val BASE_URL = "https://api.themoviedb.org"
const val API_KEY = "e54720505799206ec2429c63a68a0e2f"

object StandardDateFormat: SimpleDateFormat("dd.MM.yyyy", Locale.getDefault())
object DiaryNoteDateFormat: SimpleDateFormat("dd MMMM yyyy", Locale.getDefault())
object DiaryTimeFormat: SimpleDateFormat("HH:mm", Locale.getDefault())

object LocalBroadcast {
    val BACK: String = "BACK"
    val CONECTION_ERROR = "CONECTION_ERROR"

    private val broadcastReceivers = HashMap<IAction, BroadcastReceiver>()


    fun sendBroadcast(context: Context, action: String) {
        LocalBroadcastManager.getInstance(context).sendBroadcast(Intent(action))
    }


    fun register(
        context: Context,
        lifecycleOwner: LifecycleOwner,
        iAction: IAction,
        vararg actions: String?
    ) {
        val broadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                iAction.onAction(intent.action)
            }
        }
        val intentFilter = IntentFilter()
        for (action in actions) {
            intentFilter.addAction(action)
        }
        val localBroadcastManager = LocalBroadcastManager.getInstance(context)
        lifecycleOwner.lifecycle.addObserver(object : LifecycleEventObserver {
            override fun onStateChanged(source: LifecycleOwner, event: Lifecycle.Event) {
                when (event) {
                    Lifecycle.Event.ON_PAUSE -> localBroadcastManager.unregisterReceiver(
                        broadcastReceiver
                    )
                    Lifecycle.Event.ON_RESUME -> localBroadcastManager.registerReceiver(
                        broadcastReceiver,
                        intentFilter
                    )
                    Lifecycle.Event.ON_DESTROY -> lifecycleOwner.lifecycle.removeObserver(this)
                    else -> {}
                }
            }
        })
    }

    fun register(context: Context, iAction: IAction, vararg actions: String) {
        var broadcastReceiver = broadcastReceivers[iAction]
        if (broadcastReceiver != null) {
            LocalBroadcastManager.getInstance(context).unregisterReceiver(broadcastReceiver)
        }
        broadcastReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                iAction.onAction(intent.action)
            }
        }
        val localBroadcastManager = LocalBroadcastManager.getInstance(context)
        val intentFilter = IntentFilter()
        for (action in actions) {
            intentFilter.addAction(action)
        }
        localBroadcastManager.registerReceiver(broadcastReceiver, intentFilter)
        broadcastReceivers[iAction] = broadcastReceiver
    }

    fun unregister(context: Context, iAction: IAction) {
        val broadcastReceiver = broadcastReceivers.remove(iAction)
        if (broadcastReceiver != null) LocalBroadcastManager.getInstance(context)
            .unregisterReceiver(broadcastReceiver)
    }

    interface IAction {
        fun onAction(action: String?)
    }
}

object ToastManager {
    private var currentToast: Toast? = null

    fun showToast(context: Context, message: String) {
        currentToast?.cancel()
        currentToast = Toast.makeText(context, message, Toast.LENGTH_LONG).also {
            it.show()
        }
    }

    fun showToast(context: Context, message: String, length: Int) {
        currentToast?.cancel()
        currentToast = Toast.makeText(context, message, length).also {
            it.show()
        }
    }

    fun showToast(context: Context, message: Int) {
        currentToast?.cancel()
        currentToast = Toast.makeText(context, message, Toast.LENGTH_LONG).also {
            it.show()
        }
    }

    fun showToast(context: Context, message: Int, length: Int) {
        currentToast?.cancel()
        currentToast = Toast.makeText(context, message, length).also {
            it.show()
        }
    }
}