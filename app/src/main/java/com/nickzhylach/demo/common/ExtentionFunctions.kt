package com.nickzhylach.demo.common

import android.app.Activity
import android.content.Context
import android.database.Cursor
import android.net.ConnectivityManager
import android.net.Uri
import android.provider.MediaStore
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.karumi.dexter.listener.single.PermissionListener

fun Uri.getPathExt(context: Context): String? {
    val projection = arrayOf(MediaStore.Images.Media.DATA)
    val cursor: Cursor? =
            context.contentResolver.query(this, projection, null, null, null) ?: return null
    val columnIndex: Int? = cursor?.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
    cursor?.moveToFirst()
    val s: String? = columnIndex?.let { return@let cursor.getString(it) }
    cursor?.close()
    return s
}

fun View.keyBoardDown(activity: Activity) {
    val imm: InputMethodManager =
            activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(
        this.windowToken,
        InputMethodManager.HIDE_NOT_ALWAYS
    )
}

fun Context.isInternetConnected(): Boolean {
    val cm: ConnectivityManager? = this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val networkInfo = cm?.allNetworks
    return networkInfo?.isNotEmpty() ?: false
}

fun Context.checkPermission(
    permission: String,
    onGranted: () -> Unit,
    onDenied: () -> Unit,
    onRationaleShouldBeShown: () -> Unit
) {
    Dexter.withContext(this)
            .withPermission(permission)
            .withListener(object : PermissionListener {
                override fun onPermissionGranted(p0: PermissionGrantedResponse?) {
                    onGranted()
                }

                override fun onPermissionDenied(p0: PermissionDeniedResponse?) {
                    onDenied()
                }

                override fun onPermissionRationaleShouldBeShown(
                    p0: PermissionRequest?,
                    p1: PermissionToken?
                ) {
                    p1?.continuePermissionRequest()
                    onRationaleShouldBeShown()
                }
            })
            .check()
}

fun Context.checkPermissions(
    permissions: ArrayList<String>,
    onChecked: () -> Unit,
    onRationaleShouldBeShown: () -> Unit
) {
    Dexter.withContext(this)
            .withPermissions(permissions)
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(p0: MultiplePermissionsReport?) {
                    onChecked()
                }

                override fun onPermissionRationaleShouldBeShown(
                    p0: MutableList<PermissionRequest>?,
                    p1: PermissionToken?
                ) {
                    p1?.continuePermissionRequest()
                    onRationaleShouldBeShown()
                }
            })
            .check()
}