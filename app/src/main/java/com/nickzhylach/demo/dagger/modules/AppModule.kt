package com.nickzhylach.demo.dagger.modules

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.nickzhylach.demo.services.ISearchService
import com.nickzhylach.demo.services.SearchService
import dagger.Binds
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [ApplicationModuleBinds::class])
object AppModule {
    @Singleton
    @JvmStatic
    @Provides
    fun gson(): Gson = GsonBuilder()
        .setLenient()
        .setDateFormat("yyyy-MM-dd HH:mm:ss").create()

    /*@Singleton
    @JvmStatic
    @Provides
    fun providePreferencesRepository(context: Context, gson: Gson): PreferencesRepository =
        PreferencesRepository(context, "App_Data", gson)*/
}

@Module
abstract class ApplicationModuleBinds {
//    @Binds
//    @Singleton
//    abstract fun bindLogService(logService: LogService): ILogService
//
    @Binds
    @Singleton
    abstract fun bindDataService(searchService: SearchService): ISearchService
//
//    @Binds
//    @Singleton
//    abstract fun provideSettingsService(settingsService: SettingsService): ISettingsService
}
