package com.nickzhylach.demo.dagger.modules

import com.nickzhylach.demo.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module(includes = [BeginningModule::class])
abstract class ActivityModule {
    @ContributesAndroidInjector
    abstract fun contributeActivityAndroidInjector(): MainActivity
}