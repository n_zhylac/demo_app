package com.nickzhylach.demo.dagger.modules

import androidx.lifecycle.ViewModel
import com.nickzhylach.demo.dagger.ViewModelBuilder
import com.nickzhylach.demo.dagger.ViewModelKey
import com.nickzhylach.demo.ui.findByActor.FindByFragment
import com.nickzhylach.demo.ui.findByActor.FindByActorViewModel
import com.nickzhylach.demo.ui.foundMovies.FoundMoviesFragment
import com.nickzhylach.demo.ui.foundMovies.FoundMoviesViewModel
import com.nickzhylach.demo.ui.movie.MovieFragment
import com.nickzhylach.demo.ui.movie.MovieViewModel
import com.nickzhylach.demo.ui.start.StartFragment
import com.nickzhylach.demo.ui.start.StartViewModel
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
abstract class BeginningModule {
    @ContributesAndroidInjector(modules = [
        ViewModelBuilder::class
    ])
    internal abstract fun startFragment(): StartFragment

    @ContributesAndroidInjector(modules = [
        ViewModelBuilder::class
    ])
    internal abstract fun movieFragment(): MovieFragment

    @ContributesAndroidInjector(modules = [
        ViewModelBuilder::class
    ])
    internal abstract fun foundMoviesFragment(): FoundMoviesFragment

    @ContributesAndroidInjector(modules = [
        ViewModelBuilder::class
    ])
    internal abstract fun findByActorFragment(): FindByFragment

    @Binds
    @IntoMap
    @ViewModelKey(StartViewModel::class)
    abstract fun bindStartViewModel(viewModel: StartViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MovieViewModel::class)
    abstract fun bindLoginViewModel(viewModel: MovieViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(FoundMoviesViewModel::class)
    abstract fun bindRegistrationViewModel(viewModel: FoundMoviesViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(FindByActorViewModel::class)
    abstract fun bindSmsVerificationViewModel(viewModel: FindByActorViewModel): ViewModel
}