package com.nickzhylach.demo.dagger.components

import android.content.Context
import com.nickzhylach.demo.App
import com.nickzhylach.demo.dagger.modules.ActivityModule
import com.nickzhylach.demo.dagger.modules.AppModule
import com.nickzhylach.demo.dagger.modules.NetModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AppModule::class,
    NetModule::class,
    AndroidSupportInjectionModule::class,
    ActivityModule::class
])
interface AppComponent : AndroidInjector<App> {
    @Component.Factory
    interface Factory {
        fun create(@BindsInstance applicationContext: Context): AppComponent
    }
}